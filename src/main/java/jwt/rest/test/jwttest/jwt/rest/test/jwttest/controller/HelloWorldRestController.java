package jwt.rest.test.jwttest.jwt.rest.test.jwttest.controller;

import jwt.rest.test.jwttest.jwt.rest.test.jwttest.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldRestController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @RequestMapping({"/hello"})
    public String firstPage(@RequestHeader("Authorization") String auth){
        System.out.println("AUTHORIZATION");
        System.out.println(auth);

        String username = jwtTokenUtil.getUsernameFromToken(auth.substring(7));
        System.out.println("User name found = " + username);

        return "Hello World";
    }

}
