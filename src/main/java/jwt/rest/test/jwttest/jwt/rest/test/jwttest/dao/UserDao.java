package jwt.rest.test.jwttest.jwt.rest.test.jwttest.dao;

import jwt.rest.test.jwttest.jwt.rest.test.jwttest.model.DAOUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDao extends CrudRepository<DAOUser, Integer> {
    DAOUser findByUsername(String username);
}